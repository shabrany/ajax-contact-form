(function($) {
    $(document).ready(function() {
        
        $('.ajax-contact-form').on('submit', handleRequest);

        function handleRequest(e) {
            e.preventDefault();
            var formData = $(this).serialize();
            $.post(this.action, formData, function(r) {                
                if (r.success === 0 && r.errors) {
                    displayError(r.errors);
                }
                if (r.success === 1) {
                    showSuccessMessage(r);
                } 
            });    
        }

        function showSuccessMessage(r) {
            var speed = 350;
            var $div = $('<div>', { class: 'ajax-contact-message ' + r.type_message }).hide();
            $('.ajax-contact-form').before($div.text(r.message));
            $div.slideDown(speed, function() {
                if (r.type_message == 'success')
                    $('.ajax-contact-form').fadeOut(speed);
            });
        }

        function displayError(errors) {     
            $('.input-error').remove();
            for (var key in errors) {
                var $span = $('<span>', { class: 'input-error' });
                $span.text(errors[key]);
                $('#' + key).after($span);                
            }
        }
    });
})(jQuery);