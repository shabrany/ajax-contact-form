
Simple Contact Form with AJAX validaiton. 
E-mails are also sent with AJAX.
Contact form is also mobile responsive. 
You can style the form by creating a extra css file and overriding his classes


## Features
---
- AJAX server side validation
- AJAX display errors by field
- Mobile responsive
- Configurate any thing about the file in just one setting


## Installation
---
1. Copy the contact.css file to your CSS folder 
2. Copy contact.js file to your JS folder 
3. Include the next code in your header
   
    <script src="js/jquery.js"></script> <!-- jQuery library -->
    <script src="js/contact.js"></script>
    <link rel="stylesheet" type="text/css" href="css/contact.css">

4. Include the folder inc in your webroot
5. Modify the settings in inc/config.php to your needs
6. Copy the only contact form in contact.php and paste it in the page you want the put contact form
7. You are ready to go!


## Questions
---
If you any questions about this code, dont doubt to contact to yoyo.armani@gmail.com



