<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags for mobile responsive -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AJAX Mobile Responsive Contact Form</title>

    <!-- Required -->
    <script src="js/jquery.js"></script>
    <script src="js/contact.js"></script>
    <link rel="stylesheet" type="text/css" href="css/contact.css">
    <!-- /Required -->

    <!-- This is only for the demo -->
    <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>     -->
    <link rel="stylesheet" type="text/css" href="css/demo.css">

</head>
<body>
    <div class="wrapper">


        <h1>AJAX Contact form</h1>

        <!-- Copy only this -->

        <form class="ajax-contact-form" action="inc/handle.php" method="post">
            <div class="special-field">
                <input type="text" name="specialfield" id="specialfield" value="">
            </div>
            <div class="field">
                <label>Name</label>
                <div class="field-input">
                    <input type="text" name="name" id="name" value="" class="input">
                </div>                
            </div>
            <div class="field">
                <label>Phone</label>
                <div class="field-input">
                    <input type="text" name="phone" id="phone" value="" class="input">
                </div>                
            </div>
            <div class="field">
                <label>E-mailaddress</label>
                <div class="field-input">
                    <input type="email" name="email" id="email" value="" class="input">
                </div>                
            </div>
            <div class="field">
                <label>Subject</label>
                <div class="field-input">
                    <input type="text" name="subject" id="subject" value="" class="input">
                </div>                
            </div>
            <div class="field">
                <label>Message</label>
                <div class="field-input">
                    <textarea name="message" id="message" class="input"></textarea>
                </div>                
            </div>
            <div class="field submit">
                <div class="field-input">
                    <button type="submit">Send</button>
                </div>                
            </div>
        </form>

        <!-- / Copy only this -->

    </div>
</body>
</html>