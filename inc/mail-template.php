<!DOCTYPE html>
<html>
<body>
    <p>There is a new contact message sent from <?php echo $website ?>.</p>

    <table>
        <?php foreach ($form_data as $key => $value): ?>
        <tr>
            <td valign="top" width="100"><b><?php echo ucfirst($key); ?>:</b></td>
            <td valign="top"><?php echo $value; ?></td>
        </tr>     
        <?php endforeach; ?>   
    </table>
</body>
</html>