<?php 
/**
 * If you know what you are doing. You can modify this file
 * By getting problems after modifying this file, any kind 
 * of support would not be given.
 *
 * 
 * 
 */

$config = require_once( __DIR__ . '/config.php');
$form_data = [];
$errors = [];
$response =['success' => 0, 'message' => 'Bad request'];

//print_r($_POST); die();

# call only realised by XHR
if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['']) {
    echo 'Bad request';
    die();
}

# Check first bots 
if (trim($_POST['specialfield']) === '') {

    # Validate fields
    foreach ($_POST as $key => $value) {
        if ($key === 'email' && in_array($key, $config['required'])) {
            if (!filter_input(INPUT_POST, $key, FILTER_VALIDATE_EMAIL))
                $errors[$key] = $config['validation_message']['email'];
        }

        if (trim($value) === '' && in_array($key, $config['required'])) {        
            $errors[$key] = $config['validation_message']['required'];
        }
        
        $form_data[$key] = $value;      
    }

    # Check for errors
    if (count($errors)) {
        $response['message'] = 'Errors found';
        $response['errors'] = $errors;
    } else {
        # Get mail template 
        ob_start();
        $website = $_SERVER['HTTP_ORIGIN'];
        require_once( __DIR__ . '/mail-template.php');
        $message = ob_get_contents();
        ob_end_clean();

        # mail headers
        $headers = "From: {$form_data['name']} <{$form_data['email']}>" . "\r\n";
        $headers.= "MIME-Version: 1.0" . "\r\n";
        $headers.= "Content-type: text/html; charset: UTF-8" . "\r\n";    

        # Subject
        $subject = (isset($form_data['subject']) && !empty($form_data['subject'])) ? $form_data['subject'] : $config['subject'];
          
        # Send email
        if (($sent = mail($config['to'], $subject, $message, $headers))) {
            $response['message'] = $config['success_message'];
            $response['type_message'] = 'success';
            $response['success'] = 1;
        } else {
            $response['message'] = 'Your message could not be sent';
            $response['type_message'] = 'error';
        }
    }

} else {
    $response['message'] = 'No bots allowed';
    $response['type_message'] = 'error';
}

header('Content-type: application/json; charset: UTF-8');
print json_encode($response);