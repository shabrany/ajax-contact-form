<?php return array (

    /**
     * Set here the e-mailaddress where the contact message will be sent to
     * 
     */
    'to' => 'yoyo.armani@gmail.com',

    /**
     * Default subject message. The subject in the form will override
     * this message
     * 
     */
    'subject' => 'New contact message',

    /**
     * Fill the names that must be filled in by the visitor
     * Available: name, email, phone, subject, message
     * 
     */
    'required' => ['name', 'email', 'message'],

    /**
     * Message that should be display to the visistor after
     * the contact form has been submitted
     * 
     */
    'success_message' => 'Your message has been sent',

    /**
     * In this seciton you can modify the messages for the fields
     * that are required en the message for an invalid e-mailaddress
     * 
     */
    'validation_message' => [

        'required' => 'This field is required',

        'email' => 'This e-mailaddress is invalid'
    ]

);